<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DB;

class CategoryController extends Controller
{
    public function index(){
    	return view('admin.category.add-category');
    }
    //Category form validatiopn
  
    public function saveCategory(Request $request){
    
    	$this->validate($request, [
    		'category_name'         => 'required|regex:/^[\pL\s\-]+$/u|max:10|min:4',
   			'description'           => 'required',
   			'publication_status'    => 'required',
    	]);
    	$category = new Category();

    	$category->category_name =$request->category_name;
    	$category->description =$request->description;
    	$category->publication_status =$request->publication_status;
    	$category->save();

    	/*
		$result= $request->all();
    	echo "<pre>";
    	print_r($result);
    	

    	Category::create($request->all());
    	*/
    	return redirect('/category/add')->with('message', 'Category info save successfully');
    }

    public function manageCategory(){

    	$categories =Category::paginate(10);
    	/*
			echo "</pre>";
			print_r($categories);
    	*/
    	
    	return view('admin.category.manage-category', ['categories'=>$categories]);
    }

    public function publishedCategoryInfo($id){
    	$category = Category::find($id);
    	$category->publication_status=1;
    	$category->save();
    	
        return redirect('/category/manage')->with('message','Category info Published');
    }
    public function unpublishedCategoryInfo($id){

    	$category =Category::find($id);
    	$category->publication_status=0;
    	$category->save();

    	return redirect('/category/manage')->with('message','Category info UnPublished');
    }
    public function editCategoryInfo($id){

        $category = Category::where(['id'=>$id])->first();

      return view('admin.category.edit-category',['category'=>$category ]);
    }

    public function updateCategoryInfo(Request $request){
       // return $request->all();
	   $this->validate($request, [
		   'category_name'         => 'required|regex:/^[\pL\s\-]+$/u|max:10|min:4',
		   'description'           => 'required',
		   'publication_status'    => 'required',
	   ]);
        $category =Category::find($request->id);
        $category->category_name =$request->category_name;
        $category->description =$request->description;
        $category->publication_status =$request->publication_status;
        $category->save();

        return redirect('/category/manage')->with('message', 'Category info update Successfully');

    }

    public function deleteCategory($id){
        $category =Category::find($id);
        $category->delete();

        return redirect('/category/manage')->with('message','Category info delete Successfully');
    }
}
