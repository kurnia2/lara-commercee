<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/

/*E-commerce Front-end mastering Start*/

Route::get('/', [ 

	'uses'  => 'ShopController@index',
	'as'    =>'/'
 ]);

Route::get('/product', [
	'uses' =>'ShopController@product',
	'as'   =>'/product'
]);

Route::get('/product-details',[

	'uses'=>'ShopController@productdetail',
	'as'  =>'/product-details'
]);
Route::get('/cart',[
	'uses'=>'ShopController@cart',
	'as'  =>'/cart'
]);
Route::get('/checkout',[
	'uses'=>'ShopController@checkout',
	'as'  =>'/checkout'
]);
/*
Route::post('/user-login',[
	'uses'=>'ShopController@login',
	'as'  =>'/user-login'
]);
*/
/*E-commerce Front-end mastering End*/
/*E-commerce Back-end mastering start*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/* Category Route Start */

Route::get('category/add',[
		'uses'=>'CategoryController@index',
		'as'  =>'add-category'
]);
Route::post('category/new-category',[
		'uses'=>'CategoryController@saveCategory',
		'as'  =>'new-category'
]);
Route::get('category/manage',[
		'uses'=>'CategoryController@manageCategory',
		'as'  =>'manage-category'
]);

Route::get('/category/published/{id}',[
		'uses'=>'CategoryController@publishedCategoryInfo',
		'as'  =>'published-category'
]);
Route::get('/category/unpublished/{id}',[
		'uses'=>'CategoryController@unpublishedCategoryInfo',
		'as'  =>'unpublished-category'
]);
Route::get('/category/edit/{id}',[
		'uses' =>'CategoryController@editCategoryInfo',
		'as'   =>'edit-category'
]);
Route::post('/category/update',[
		'uses' =>'CategoryController@updateCategoryInfo',
		'as'   =>'update-category'
]);
Route::get('/category/delete/{id}',[
		'uses' =>'CategoryController@deleteCategory',
		'as'   =>'delete-category'
]);
/* Category Route End */

/* Brand Route Start */

Route::get('brand/add',[
		'uses' =>'BrandController@index',
		'as'   =>'add-brand'
]);
Route::post('brand/save',[

		'uses' =>'BrandController@saveBrandInfo',
		'as'   =>'new-brand'
]);
Route::get('/brand/manage',[
		'uses' =>'BrandController@manageBrandInfo',
		'as'   =>'manage-brand'
]);
Route::get('/brand/published/{id}',[
	 'uses'  =>'BrandController@publishedBrandInfo',
	 'as'    =>'published-brand'
]);
Route::get('/brand/unpublished/{id}', [
	'uses' =>'BrandController@unpublishedBrandInfo',
	'as'    =>'unpublished-brand'
]);
Route::get('/brand/edit/{id}',[
	'uses' =>'BrandController@editBrandInfo',
	'as'   =>'edit-brand'
]);
Route::post('/brand/update',[
	'uses'=>'BrandController@updateBrandInfo',
	'as'  =>'update-brand'
]);
Route::get('/brand/delete/{id}',[
	'uses' =>'BrandController@deleteBrandInfo',
	'as'   =>'delete-brand'
]);
/* Brand Route End */

/* Product Route Start */
Route::get('product/add',[
	'uses' =>'ProductController@index',
	'as'   =>'add-product'
]);

Route::post('product/save', [
	'uses' =>'ProductController@saveProductInfo',
	'as'   =>'new-product'
]);
Route::get('product/manage',[
	'uses' =>'ProductController@manageProductInfo',
	'as'   =>'manage-product'
]);
Route::get('product/edit/{id}',[
	'uses' =>'ProductController@editProductInfo',
	'as'   =>'edit-product'
]);
Route::post('product/update',[
	'uses' =>'ProductController@updateProductInfo',
	'as'   =>'update-product'
 ]);

/*Attribute Functionality*/
 Route::get('attribute/add/{id}',[

	'uses' =>'ProductController@addAttribute',
	'as'   =>'add-attribute'
 ]);
/* Product Route End */
/*E-commerce Back-end mastering End*/ 