<!DOCTYPE html>
<html lang="en">
<head>
<title>E-commerce Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/bootstrap.min.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/fullcalendar.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/uniform.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/matrix-style.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/matrix-media.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/select2.css" />
<link href="{{ asset('/') }}admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/jquery.gritter.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset('/') }}admin/css/bootstrap.min.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/uniform.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/select2.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/matrix-style.css" />
<link rel="stylesheet" href="{{ asset('/') }}admin/css/matrix-media.css" />
<link href="{{ asset('/') }}admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<!--Header section-->
@include('admin.include.header')
@include('admin.include.sidebar-menu')
<!--sidebar-menu-->


<!--main-container-part-->

@yield('body')


<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>

<!--end-Footer-part-->
<script src="{{asset('/')}}admin/js/jquery.min.js"></script> 
<script src="{{ asset('/')}}admin/js/jquery.ui.custom.js"></script> 
<script src="{{ asset('/')}}admin/js/bootstrap.min.js"></script> 
<script src="{{asset('/')}}admin/js/jquery.uniform.js"></script> 
<script src="{{asset('/')}}admin/js/select2.min.js"></script> 
<script src="{{asset('/')}}admin/js/jquery.dataTables.min.js"></script> 
<script src="{{asset('/')}}admin/js/jquery.validate.js"></script> 
<script src="{{asset('/')}}admin/js/matrix.js"></script> 
<script src="{{asset('/')}}admin/js/matrix.form_validation.js"></script>
<script src="{{asset('/')}}admin/js/matrix.tables.js"></script>
<script src="{{asset('/')}}admin/js/matrix.popover.js"></script>



</body>
</html>
