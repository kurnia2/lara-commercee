@extends('admin.master')
@section('body')
<div id="content">
<div class="container-fluid"><hr>
	<div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add Category</h5>
          </div>
			 <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ route('new-category' )}}" name="add_category" id="add_category" novalidate="novalidate">
            	{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Category Name</label>
                <div class="controls"> 
                  <input type="text" name="category_name" id="category_name"></br>
                <span class="text-error">{{ $errors->has('category_name') ? $errors->first('category_name') : ' ' }}</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                 <textarea name="description" id="description" ></textarea></br>
                 <span class="text-error">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                </div>
              </div>
             
              <div class="control-group">
              <label class="control-label">Status</label>
               <div class="controls">
                <label>
                  <input type="radio" checked name="publication_status" value="1"/>Published</label>
                 <label> <input type="radio" checked name="publication_status" value="0"/>UnPublished</label></br>
                 <span class="text-error">{{ $errors->has('publication_status') ? $errors->first('publication_status') : ' ' }}</span>
              </div>
            </div>
         
              <div class="form-actions">
                <input type="submit" value="Add Category" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
</div> 
@endsection