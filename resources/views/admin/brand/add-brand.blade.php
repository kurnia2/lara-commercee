@extends('admin.master')
@section('body')
<div id="content">
<div class="container-fluid"><hr>
	<div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add Brand</h5>
            <h4 class="text-center text-success">{{ Session::get('message') }}</h4>
          </div>
			
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{ route('new-brand')}}" name="add_brand" id="add_brand" novalidate="novalidate">
            	{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Brand Name</label>
                <div class="controls"> 
                  <input type="text" name="brand_name" id="brand_name"></br>
                <span class="text-error">{{ $errors->has('brand_name') ? $errors->first('brand_name') : ' ' }}</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Discription </label>
                <div class="controls">
                 <textarea name="brand_dis" id="brand_dis" ></textarea></br>
                 <span class="text-error">{{ $errors->has('brand_dis') ? $errors->first('brand_dis') : ' ' }}</span>
                </div>
              </div>
             
              <div class="control-group">
              <label class="control-label">Status</label>
               <div class="controls">
                <label>
                  <input type="radio" checked name="brand_status" value="1"/>Published</label>
                 <label> <input type="radio" checked name="brand_status" value="0"/>UnPublished</label></br>
                 <span class="text-error">{{ $errors->has('brand_status') ? $errors->first('brand_status') : ' ' }}</span>
              </div>
            </div>
         
              <div class="form-actions">
                <input type="submit" value="Add Brand" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
</div> 
@endsection