@extends('admin.master')
@section('body')
<div id="content">
<div class="container-fluid"><hr>
	<div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5> Product Attribute</h5>
            <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
          </div>
			
             <div class="widget-content nopadding">

             <form action="{{ route('add-attribute',$product->id)}}" method="POST" class="form-horizontal" >
                {{ csrf_field() }}

                <div class="control-group">
                <label class="control-label">Product Name</label>
                <label class="control-label"><strong>{{$product->product_name}}</strong></label>
                </div>
                <div class="control-group">
                <label class="control-label">Product Code</label>
                <label class="control-label"><strong>{{$product->product_code}}</strong></label>
                
                </div>
                <div class="control-group">
                <label class="control-label">Product Color</label>
                <label class="control-label"><strong>{{$product->product_color}}</strong></label>
                
                </div>
                <div class="control-group">
                     <div class="field_wrapper">
                        <div style="margin-left:75px;">
                            <input type="text" name="sku[]" id="sku" placeholder="SKU" style="width:120px;"/>
                            <input type="text" name="size[]" id="size" placeholder="Size" style="width:120px;"/>
                            <input type="text" name="price[]" id="price" placeholder="Price" style="width:120px;"/>
                            <input type="text" name="stoke[]" id="stoke" placeholder="Skoke" style="width:120px;"/>
                           <a href="javascript:void(0);" class="add_button" title="Add field">Add</a>
                         </div>
                     </div>
                    
                </div>
                <div class="form-actions">
                <button type="submit" class="btn btn-success">Save Product</button>
                </div>
            </form>

        </div>
      </div>
        </div>
      </div>
    </div>
</div>
</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div style="margin-left:75px; margin-right:3px;"> <input type="text" name="sku[]" id="sku" placeholder="SKU" style="width:120px; margin-right:3px;"/> <input type="text" name="size[]" id="size" placeholder="Size" style="width:120px;"/>  <input type="text" name="price[]" id="price" placeholder="Price" style="width:120px;"/><input type="text" name="stoke[]" id="stoke" placeholder="Skoke" style="width:120px;"/> <a href="javascript:void(0);" class="remove_button">Remove</a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
@endsection