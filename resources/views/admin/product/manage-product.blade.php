@extends('admin.master')
@section('body')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Products Tables</a> </div>
    <h1 class="text-center text-info">Products Tables</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Product table</h5>
            <h4 class="text-center text-success">{{ Session::get('message')}}</h4>
            <h4 class="text-center text-success"></h4>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Product Id</th>
                  <th>Brand Name</th>
                  <th>Category Name</th>
                  <th>Product Name</th>
                  <th>Product code</th>
                  <th>Product Color</th>
                  <th>Product Price</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              
                @foreach ($products as $product)
                <tr class="gradeX">
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->brand_name}}</td>
                    <td>{{ $product->category_name}}</td>
                    <td>{{ $product->product_name}}</td>
                    <td>{{ $product->product_code}}</td>
                    <td>{{ $product->product_color}}</td>
                    <td>{{ $product->price}}</td>
                    <td><img src="{{ asset('/products/small/'.$product->image)}}" alt="" height="100" width="100"></td>
                    
                    <td>
                      <a href="#myModal{{$product->id}}" data-toggle="modal" class="btn btn-warning btn-xs">
                      <i class="icon-eye-open"></i>
                      </a>
                      <a href="{{ route('edit-product',['id'=>$product->id])}}" class="btn btn-primary btn-xs">
                      <i class="icon-edit"></i>
                      </a>
                      <a href="{{ route('add-attribute',['id'=>$product->id])}}" class="btn btn-success  btn-xs">
                        <i class="icon-plus"></i>
                        </a>
                      <a href="" class="btn btn-danger btn-xs">
                      <i class="icon-trash"></i>
                      </a>
                    </td>
                  </tr>
                  <div id="myModal{{$product->id}}" class="modal hide">
                    <div class="modal-header">
                      <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3 class="text-center text success">{{ $product->product_name}}</h3>
                    </div> 
                    <div class="modal-body">
                      <p>Product ID: {{ $product->id }}</p>
                      <p>Brand Name: {{ $product->brand_name}}</p>
                      <p>Product Name: {{ $product->category_name}}</p>
                      <p>Product Name: {{ $product->product_name}}</p>
                      <p>Product Code: {{ $product->product_code}}</p>
                      <p>Product Color: {{ $product->product_color}}</p>
                      <p>Product Price: {{ $product->price}}</p>
                      <p>Product Image: <img src="{{ asset('/products/small/'.$product->image)}}" alt="" height="100" width="100"></p>
                      <p>Product Description: {{ $product->description}}</p>
                    </div>
                  </div>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="text-right">
            <ul class="pagination">
              <li class="active">
             
              </li>  
            </ul>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>

@endsection