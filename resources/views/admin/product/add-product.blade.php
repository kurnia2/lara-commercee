@extends('admin.master')
@section('body')
<div id="content">
<div class="container-fluid"><hr>
	<div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add Product</h5>
            <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
          </div>
			
             <div class="widget-content nopadding">
             <form action="{{ route('new-product')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
               {{ csrf_field() }}
            
               <div class="control-group">
                <label class="control-label">Product Brand</label>
                <div class="controls">
                  <select name="brand_id">
                    <option>---Select Brand Name---</option>
                    @foreach ($brands as $brand)
                    <option value="{{ $brand->id}}">{{ $brand->brand_name }}</option>
                    @endforeach
                   
                  </select></br>
                <span class="text-danger">{{ $errors->has('brand_name') ? $errors->first('brand_name') : ' ' }}</span>
                </div>
              </div>
            <div class="control-group">
              <label class="control-label">Product Category</label>
              <div class="controls">
                <select name="category_id">
                  <option>---Select Category name---</option> 
                  @foreach ($categories as $category)
                  <option value="{{ $category->id}}"> {{ $category->category_name}} </option> 
                  @endforeach
                 
                </select></br>
                <span class="text-danger">{{ $errors->has('category_name') ? $errors->first('category_name') : ' ' }}</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Product Name</label>
              <div class="controls"> 
                <input type="text" name="product_name" id="product_name"></br>
              <span class="text-error">{{ $errors->has('product_name') ? $errors->first('product_name') : ' ' }}</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Product Code</label>
              <div class="controls"> 
                <input type="text" name="product_code" id="product_code"></br>
              <span class="text-error">{{ $errors->has('product_code') ? $errors->first('product_code') : ' ' }}</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Product Color</label>
              <div class="controls"> 
                <input type="text" name="product_color" id="product_color"></br>
              <span class="text-error">{{ $errors->has('product_color') ? $errors->first('product_color') : ' ' }}</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Product Discription </label>
              <div class="controls">
               <textarea name="description" id="description" ></textarea></br>
               <span class="text-error">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Product Price</label>
              <div class="controls"> 
                <input type="text" name="price" id="price"></br>
              <span class="text-error">{{ $errors->has('price') ? $errors->first('price') : ' ' }}</span>
              </div>
            </div>
           
           <div class="control-group">
              <label class="control-label">Upload Image </label>
              <div class="controls">
                <input name="image" type="file" /> </br>
                <span class="text-error">{{ $errors->has('image') ? $errors->first('image') : ' ' }}</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Publication Status</label>
              <div class="controls">
                <label>
                  <input type="radio" name="product_status" value="1"/>
                 Published</label>
                <label>
                  <input type="radio" name="product_status" value="0" />
                  unPublished</label>
                <label>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save Product</button>
            </div>
          </form>

        </div>
      </div>
        </div>
      </div>
    </div>
</div>
</div> 
@endsection