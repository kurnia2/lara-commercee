<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="active"><a href="{{ route('home') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <!--
    <li> <a href="charts.html"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a> </li>
    <li> <a href="widgets.html"><i class="icon icon-inbox"></i> <span>Widgets</span></a> </li>
    <li><a href="tables.html"><i class="icon icon-th"></i> <span>Tables</span></a></li>
    <li><a href="grid.html"><i class="icon icon-fullscreen"></i> <span>Full width</span></a></li>
  -->
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Category</span></a>
      <ul>
        <li><a href="{{ route('add-category') }}">Add Category</a></li>
        <li><a href="{{ route('manage-category')}}">Manage Category</a></li>
       
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-tint"></i> <span>Brand</span></a>
      <ul>
        <li><a href="{{ route('add-brand')}}">Add Brand</a></li>
        <li><a href="{{ route('manage-brand')}}">Manage Brand</a></li>
       
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Product</span></a>
      <ul>
        <li><a href="{{ route('add-product') }}">Add Product</a></li>
      <li><a href="{{ route('manage-product') }}">Manage Product</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Cuppon</span></a>
      <ul>
        <li><a href="#">Add Cuppon</a></li>
        <li><a href="#">Manage Cuppon</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Slider</span></a>
      <ul>
        <li><a href="#">Add Slider</a></li>
        <li><a href="#">Manage Slider</a></li>
      </ul>
    </li>
  </ul>
</div>
<!--sidebar-menu-->